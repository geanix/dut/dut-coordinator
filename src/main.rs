#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate log;
#[macro_use]
extern crate rocket_contrib;

use diesel::result::DatabaseErrorKind;
use diesel::result::Error;
use diesel::SqliteConnection;
use rocket::fairing::AdHoc;
use rocket::http::Status;
use rocket::Rocket;
use rocket_contrib::json::Json;

pub mod schema;

pub mod device;
use device::Device;

// This macro from `diesel_migrations` defines an `embedded_migrations` module
// containing a function named `run`. This allows the example to be run and
// tested without any outside setup of the database.
embed_migrations!();

#[database("dut_db")]
pub struct DbConn(SqliteConnection);

#[get("/devices")]
fn get_devices(conn: DbConn) -> Result<Json<Vec<String>>, String> {
    let mut v = Vec::new();
    match Device::all(&conn) {
        Ok(devices) => {
            for device in devices {
                v.push(device.name);
            }
            Ok(Json(v))
        }
        Err(e) => Err(format!("DB query error: {}", e)),
    }
}

#[get("/devices/<name>")]
fn get_device(name: String, conn: DbConn) -> Result<Option<Json<Device>>, String> {
    match Device::get(&name, &conn) {
        Ok(dev) => Ok(Some(Json(dev))),
        Err(Error::NotFound) => Ok(None),
        Err(e) => Err(format!("DB query error: {}", &e)),
    }
}

#[post("/devices", format = "json", data = "<device>")]
fn post_devices(device: Json<Device>, conn: DbConn) -> Status {
    let device = device.into_inner();
    match Device::insert(device, &conn) {
        Err(Error::DatabaseError(DatabaseErrorKind::UniqueViolation, _)) => Status::Conflict,
        Err(e) => {
            error!("DB insertation error: {}", e);
            Status::InternalServerError
        }
        Ok(_) => Status::Created,
    }
}

#[delete("/devices/<name>")]
fn delete_devices(name: String, conn: DbConn) -> Result<Option<()>, String> {
    match Device::delete(&name, &conn) {
        Err(e) => Err(format!("DB deletion error: {}", e)),
        Ok(0) => Ok(None),
        Ok(_) => Ok(Some(())),
    }
}

fn run_db_migrations(rocket: Rocket) -> Result<Rocket, Rocket> {
    let conn = DbConn::get_one(&rocket).expect("database connection");
    match embedded_migrations::run(&*conn) {
        Ok(()) => Ok(rocket),
        Err(e) => {
            error!("Failed to run database migrations: {:?}", e);
            Err(rocket)
        }
    }
}

fn rocket() -> Rocket {
    rocket::ignite()
        .attach(DbConn::fairing())
        .attach(AdHoc::on_attach("Database Migrations", run_db_migrations))
        .mount(
            "/",
            routes![get_devices, get_device, post_devices, delete_devices],
        )
}

fn main() {
    rocket().launch();
}
