use diesel::{self, prelude::*, result::QueryResult, sqlite::SqliteConnection};
use serde::{Deserialize, Serialize};

use crate::schema::device_parameters;
use crate::schema::devices;

#[derive(Serialize, Deserialize, Queryable, Insertable, Debug, Clone)]
#[table_name = "devices"]
pub struct Device {
    pub name: String,
}

#[derive(Serialize, Deserialize, Queryable, Associations, PartialEq, Debug, Clone)]
#[belongs_to(Device, foreign_key = "device_name")]
#[table_name = "device_parameters"]
pub struct Parameter {
    pub device_name: String,
    pub key: String,
    pub value: String,
}

impl Device {
    pub fn all(conn: &SqliteConnection) -> QueryResult<Vec<Device>> {
        devices::table.load::<Device>(conn)
    }

    pub fn insert(device: Device, conn: &SqliteConnection) -> QueryResult<usize> {
        diesel::insert_into(devices::table)
            .values(&device)
            .execute(conn)
    }

    pub fn delete(name: &str, conn: &SqliteConnection) -> QueryResult<usize> {
        diesel::delete(devices::table.filter(devices::name.eq(name))).execute(conn)
    }

    pub fn get(name: &str, conn: &SqliteConnection) -> QueryResult<Device> {
        devices::table.filter(devices::name.eq(name)).first(conn)
    }
}
