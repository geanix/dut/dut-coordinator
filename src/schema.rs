table! {
    devices (name) {
        name -> Text,
    }
}

table! {
    device_parameters (device_name, key) {
        device_name -> Text,
        key -> Text,
        value -> Text,
    }
}
