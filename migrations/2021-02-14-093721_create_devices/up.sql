CREATE TABLE devices (
  name VARCHAR NOT NULL PRIMARY KEY
)

CREATE TABLE device_parameters (
  device VARCHAR NOT NULL,
  key VARCHAR NOT NULL,
  value VARCHAR NOT NULL,
  UNIQUE ( device, key ),
)
